<?php
// The Porter stemming algorithm
// $stem = PorterStemmer::Stem($word);
require("includes/PorterStemmer.php");

// Name of list = $stopwords
require("includes/stop-words.php");

require("includes/strip_html_tags.php");

class Page {
	
	//Attributes
	private $tf_ceiling;
	private $pageName;
	public $bagOfWords = array();
	public $cutWords = array();
	
	
	function __construct($htmlPage,$ceiling = -1,$isString = false){
		$this->pageName = $htmlPage;
		$this->tf_ceiling = $ceiling;
		$this->extract_words($htmlPage, $isString);
	}
	
	function __toString(){
		$str = $this->pageName."\n";
		foreach($this->bagOfWords as $word => $count){
			$str .= '['.$word.'] => '.$count."\n";
		}
		$str .= "------------------\n";
		return $str;
	}
	
	public function extract_words($htmlPage,$isString){		
		if($isString)
			$htmlstring = $htmlPage;
		else
			$htmlstring = file_get_contents($htmlPage);
		//Remove all tag that strip_tags does not, and line break after block-level tags
		$cleanstring = strip_html_tags($htmlstring);
		//Remove HTML-tags
		$cleanstring = strip_tags($cleanstring);
		//Decode HTML-entities
		$cleanstring = html_entity_decode($cleanstring);
		//Make the string all lower case
		$cleanstring = strtolower($cleanstring);
		//Split the string into words removing all character that are not alphanumeric
		$words = preg_split('/\W+/', $cleanstring, -1, PREG_SPLIT_NO_EMPTY);
		$words = preg_grep('/^[a-z]+$/',$words);
		
		//$words = $words=preg_split('/[^a-z]+/', $cleanstring, -1, PREG_SPLIT_NO_EMPTY);
		//Insert unique words into the global set		
		$this->bagOfWords = array_count_values($words);
		if($this->tf_ceiling > 0){
			foreach($this->bagOfWords as $word => $count){
				if($count > $this->tf_ceiling){
					$this->bagOfWords[$word] = $this->tf_ceiling;
					$this->cutWords[$word] = $count;
				}
			}
		}
	}
	
	public function sum_terms(){
		return array_sum($this->bagOfWords);
	}
	
	//Returns an array with the stemmed versions of the words in the wordbag
	public function stem_words(){
		$stemmedList = array();
		foreach($this->bagOfWords as $word => $val){
			$word = PorterStemmer::stem($word);
			if(array_key_exists($word, $stemmedList)){
				$stemmedList[$word] += $val;
			} else {
				$stemmedList[$word] = $val;
			}
		}
		return $stemmedList;
	}
	
	//Returns an array with the bagOfWords where the stop-words have been removed
	public function remove_stop_words($replace = false){
		global $stopwords;
		$noStopWordsList = array_diff_key($this->bagOfWords,$stopwords);
		if($replace)
			$this->bagOfWords = $noStopWordsList;
		return $noStopWordsList;
		
	}
	
}

?>