<?php
require("includes/Page.php");
ini_set("memory_limit","2000M");
$p_rnd_svm = "../results/random/svm/";
$ctx = stream_context_create(array(
			'http' => array(
			'timeout' => 5,
			'user_agent' => "Thesis_php_crawler"
			)
		));
/* 	(1) find_training_min_max($pathName)
		-- prints max and min weight from an SVM-traning data file to standard output.
	(2) save_manual($url_list,$wantedAmount)
	(3) partition_neg_ex($folder)
	(4) extract_all_top_cat_rdf_urls($filepath,$outfile)
	(5) extract_all_rdf_urls($filepath,$outfile)
	(6) compare_files($pathOne,$pathTwo)
	(7) find_duplicate_man($folder)
	(8) change_names($folder_path,$prefix)
	(?) create_rest_file($nameOfCategory)
		-- Create a file containing the URLs that were not used when dowloading the 5000 training examples (all_urls minus saved_urls)
	
*/

/*** What follows are some examples of use while working on the thesis ****/
//find_training_min_max($p_rnd_svm."pos_train_15_scaled.txt");
//compare_files("../results/random/svm/pos_train_15_scaled_comp.txt","../results/random/svm/pos_train_15_scaled.txt");
//change_names("../results/cooking/man_pos","1_");
//change_names("../results/cooking/man_not_pos","1_");
//find_duplicate_man("../results/martialarts/man_pos");
//extract_all_top_cat_rdf_urls("../dmoz_urls/content.rdf","../dmoz_urls/test");
//partition_neg_ex("../results/negative/url_lists/");
/*
save_manual("../results/negative/url_lists/urls_arts.txt",50);
save_manual("../results/negative/url_lists/urls_business.txt",50);
save_manual("../results/negative/url_lists/urls_computers.txt",30);
save_manual("../results/negative/url_lists/urls_games.txt",20);
save_manual("../results/negative/url_lists/urls_health.txt",20);
save_manual("../results/negative/url_lists/urls_home.txt",20);
save_manual("../results/negative/url_lists/urls_news.txt",20);
save_manual("../results/negative/url_lists/urls_recreation.txt",30);
save_manual("../results/negative/url_lists/urls_reference.txt",20);
save_manual("../results/negative/url_lists/urls_regional.txt",150);
save_manual("../results/negative/url_lists/urls_science.txt",30);
save_manual("../results/negative/url_lists/urls_shopping.txt",30);
save_manual("../results/negative/url_lists/urls_society.txt",20);
*/
//save_manual("../results/negative/url_lists/urls_sport.txt",30);
//save_manual("../results/negative/url_lists/urls_world.txt",300);
// name_by_count("../results/martialarts/man_pos");
// test_scaled("../results/christianity/svm/pos_train_15.txt.scaled");
//create_sorted_idf_az_table();


//Dowloads documents for manual categorization for the negative collection.
function save_manual($url_list,$wantedAmount){	
	$urlList = trim(file_get_contents($url_list));
	$name_n_count = explode(" ",trim(strstr($urlList,"\n",true)));
	$urlList = explode("\n",trim(strstr($urlList,"\n")));
	$size = count($urlList);
	shuffle($urlList);
	
	$saved_urls = array();
	$nbr_of_saved = 0;
	
	for($i = 0; ($i < $size) && ($nbr_of_saved < $wantedAmount); $i++){
		$url = $urlList[$i];
		//Try to access the web page
		$ret = @file_get_contents($url,0,$ctx);
		//If it failed, save in bad_urls and continue with next url
		if($ret === false){
			echo "Failed to open $url\n";
			continue;
		}
		
		//Check content type. If no content type, skip the url
		//May contain several content-type entries, the last one 
		//is the one needed in that case.
		$ct = preg_grep("/^content-type/i",$http_response_header);
		$ct = array_values($ct);
		$ct_count = count($ct);
		if($ct_count == 1){
			$type = $ct[0];
		} else if($ct_count > 1){
			$type = $ct[$ct_count-1];
		} else {
			echo "Did not find content-type for $url\n";
			continue;
		}
		
		//Make sure it's a text file, otherwise skip url.
		if(preg_match("/text\//i",$type)){
			$ret = "<!--$url-->\n".$ret;
			//If the word bag is empty, there is no point in using the page
			$page = new Page($ret,-1,true);
			$page->remove_stop_words(true);
			if(count($page->bagOfWords) <= 10){
				continue;
			}
			
			//Save file
			$nbr_of_saved++;
			$filename = "../results/negative/man/".$name_n_count[0]."_".$nbr_of_saved.".html";
			if(file_put_contents($filename,$ret) === false){
				echo "Failed to save doc nbr $nbr_of_saved\n";
				$nbr_of_saved--;
			} else {
				echo "Saved docs: $nbr_of_saved (".$name_n_count[0].")\n";
				$saved_urls[] = $url;
			}
		} else {
			echo "Not text file [$url]\n";
			continue; #URL does not point to a text file
		}
					
	}
	$rest = array_diff($urlList, $saved_urls);
	file_put_contents("../results/negative/".$name_n_count[0]."_rest_urls.txt",implode("\n",$rest));
	file_put_contents("../results/negative/".$name_n_count[0]."_saved_manual.txt",implode("\n",$saved_urls));		
	echo "\nDone!\n";
}

//This checks how many pages from each category is needed for the negative collection in order to 
//for the collection to be a good representation of the pages/category on DMOZ
function partition_neg_ex($folder){
	$tops = array();
	$sum = 0;
	$dirIt = new DirectoryIterator($folder);
	foreach($dirIt as $file){
		if($file->isDot() || ($file->getFilename() == "urls_world.txt"))
			continue;
			
		$in = fopen($file->getPathname(),"r");
		$line = fgets($in);
		$vals = explode(" ",$line);
		$tops[$vals[0]] = $vals[1];
		$sum += $vals[1];
		fclose($in);
	}
	
	$out = fopen("../results/negative/portions.txt","w");
	foreach($tops as $name => $count){
		fwrite($out,$name." ".ceil(($count/$sum)*200)."\n");
	}
	fclose($out);	
}

//Extract URLs from the DMOZ rdf-file, used for fetching pages that will be manually categorized
//as negative. Only the top-level pages are used.
function extract_all_top_cat_rdf_urls($filepath,$outfile){
	echo "Extracting urls for negative examples\n";
	$in = fopen($filepath,"r");
	$count = 0;
	$tops = array();
	while($line = fgets($in)){
		$count++;
		echo "Done line: $count     \r";
		if(preg_match('/<Topic r:id=\"Top\/([^\"^\/]+?)\/([^\"]+?)">/',$line,$match)){
			while(($linkLine = fgets($in)) && !preg_match('/<\/Topic>/', $linkLine)){
				$count++;
				echo "Done line: $count     \r";
				if(preg_match('/<link([1]?) r:resource=\"([^\"]+?)\">/',$linkLine,$linkMatch))
					$tops[$match[1]][] = $linkMatch[2];
			}
		}
	}
	fclose($in);
	echo "\nSaving urls...\n";
	foreach($tops as $name => $top){
		$out = "".count($top)."\n";
		$out .= implode("\n",$top);
		file_put_contents("../results/negative/urls_".$name.".txt",$out);
	}
	echo "Done!\n";
}

//Produces a list of all the URLs in the DMOZ rdf file. This is used to produce
//the collection of 5000 random pages.
function extract_all_rdf_urls($filepath,$outfile){
	$in = fopen($filepath,"r");
	$out = fopen($outfile,"w");
	$count = 0;
	while($line = fgets($in)){
		if(preg_match('/<ExternalPage about="(.+?)">/',$line,$match)){
			fwrite($out,$match[1]."\n");
			$count++;
			echo "Found $count urls             \r";
		}
	}
	fclose($in);
	fclose($out);
}

//Method for finding the minimum and maximum weights from a weight-vector file
function find_training_min_max($pathName){
	echo "Looking for max...\n";
		$max = 0;
		$min = 1;
		$in = fopen($pathName, "r");
		$row = 0;
		while($line = fgets($in)){
			$line = substr($line,2);
			if(empty($line))
				continue;
			$pairs = explode(" ",trim($line));
			foreach($pairs as $pair){
				$temp = explode(":",$pair);
				if($temp[1]>$max)
					$max = $temp[1];
				if($temp[1]<$min)
					$min = $temp[1];
			}
			$row++;
			echo "Row: $row, MAX = $max, MIN = $min                \r";
		}
		echo "\nDone!\n";
}

//Simple equality check
function compare_files($pathOne,$pathTwo){
	$old = md5(file_get_contents($pathOne));
	$new = md5(file_get_contents($pathTwo));
	if($old == $new)
		echo "Files are equal\n";
	else
		echo "Files are NOT equal\n";
}

//Check for duplicates among the manual example files. The first row of every 
//example-file (HTML) is the URL to that HTML-document (added by the Crawler within HTML comment tags)
//so the method searches for two identical such URLs. This is used to help make sure that there really are
//100 unique manual examples. 
function find_duplicate_man($folder){
	$urls = array();
	$dirIt = new DirectoryIterator($folder);
	foreach($dirIt as $file){
		if($file->isDot())
			continue;
		$file = fopen($file->getPathname(),"r");
		$url = trim(fgets($file));
		fclose($file);
		$url = substr($url,4,-3);
		$urls[] = $url;
	}
	$full = count($urls);
	$uniq = count(array_unique($urls));
	echo ($full-$uniq)." duplicates found!";
}

//Adds $prefix to all files in the folder at $folder_path
function change_names($folder_path,$prefix){
	echo "Renaming files to start with [$prefix]..\n";
	$dirIt = new DirectoryIterator($folder_path);
	foreach($dirIt as $file){
		if($file->isDot())
			continue;
		
		$old = $file->getFilename();
		$new = $prefix.$old;
		rename($folder_path."/".$old, $folder_path."/".$new);
	}
}

//Name all the files in the folder at $folder_path to their count number among the files
//(1.txt, 2.txt, 3.txt etc)
function name_by_count($folder_path){
	echo "Renaming files to their count number..\n";
	$dirIt = new DirectoryIterator($folder_path."_copy");
	$count = 1;
	foreach($dirIt as $file){
		if($file->isDot())
			continue;
		
		$old = $file->getFilename();
		$new = "$count.txt";
		echo "Renaming [$old] to [$new]\n";
		rename($folder_path."_copy/".$old, $folder_path."/".$new);
		$count++;
	}
}

//Find and display the max and min weight in a scaled (or any other) file
function test_scaled($filePath){
	$in = fopen($filePath,"r");
	$row = 0;
	$max = 0;
	$min = 1;
	while($line = fgets($in)){
		$line = strstr($line," ");
		if(empty($line))
			continue;
		$pairs = explode(" ",trim($line));
		foreach($pairs as $pair){
			$temp = explode(":",$pair);
			if($temp[1]>$max)
				$max = $temp[1];
			if($temp[1]<$min)
				$min = $temp[1];
		}
		$row++;
		echo "Row: $row, MAX = $max, MIN = $min      \r";
	}
}

//Converts a new-line seperated list into an array and returns the array
function table_to_array($fileName){
	$file = file_get_contents($fileName);
	if($file === false)
		trigger_error("Could not open $fileName", E_USER_ERROR);
	$file = trim($file);
	$lines = explode("\n",$file);
	$F = array();
	foreach($lines as $line){
		$pair = explode(" ",trim($line));
		if(!isset($pair[1]))
			echo "\n\n### Undefiend offset 1 in $fileName\n\n";
		$F[trim($pair[1])] = trim($pair[0]);
	}
	return $F;
}

//Creates a sorted IDF table with values > 0.3.
/*! Uses the file idf_az.txt !*/ 
function create_sorted_idf_az_table(){
	echo "Creating sorted idf_table with 100.000 values > 0.3\n";
	$idf_az = table_to_array("includes/idf_az.txt");
	$size = count($idf_az);
	echo "Sorting...\n";
	asort($idf_az);
	echo "Looking for index for which element >= 0.3...\n";
	$i = 0;
	$val = 0;
	$prev = 0;
	foreach($idf_az as $val){
		echo "Trying $val          \r";
		if($val < 0.3){
			$i++;
			$prev = $val;
		} else {
			break;
		}
	}
	echo "\nFound index for val > 0.3 => $i, previous val = $prev\n";
	$out = fopen("includes/idf_az_100k_most_common.txt","w");
	$n = 0;
	echo "Writing to file...\n";
	foreach($idf_az as $word => $val){
		if($n == (100000+$i))
			break;
		if(($i-$n) <= 0)
			fwrite($out,$val." ".$word."\n");
		$n++;
	}
	echo "Done!\n";
	fclose($out);
}

function create_rest_file($nameOfCategory){
	$all = explode("\n",trim(file_get_contents("../results/$nameOfCategory/good_urls.txt")));
	$sav = explode("\n",trim(file_get_contents("../results/$nameOfCategory/saved_urls.txt")));
	file_put_contents("../results/$nameOfCategory/rest_urls.txt",implode("\n",array_diff($all,$sav)));
}
?>