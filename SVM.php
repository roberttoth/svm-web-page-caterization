<?php
//This code was written by Robert Toth for the master thesis "Selecting Negative Examples for Training an SVM Classifier"
//Moste methods are well commented and coupled with the thesis report, it should make sense.


ini_set("memory_limit","1000M");
require("includes/Page.php");

class SVM{

	private $name;
	private $path;
		
	function __construct($aName){
		$this->name = $aName;
		$this->path = "../results/$aName";
		if(!file_exists($this->path."/svm/")){
			if(!mkdir($this->path."/svm/"))
				trigger_error("Could not create directory \"svm\" \n", E_USER_ERROR);
			else
				echo "Created directory \"svm\"\n";
		}
	}
	
	
	private function table_to_array($fileName){
		$file = file_get_contents($fileName);
		if($file === false)
			trigger_error("Could not open $fileName", E_USER_ERROR);
		$file = trim($file);
		$lines = explode("\n",$file);
		$F = array();
		foreach($lines as $line){
			$pair = explode(" ",trim($line));
			if(!isset($pair[1]))
				echo "\n\n### Undefiend offset 1 in $fileName\n\n";
			$F[trim($pair[1])] = trim($pair[0]);
		}
		return $F;
	}
	
	
	//Creates three files: IDF_table, gIDF_table and no_gIDF_words
	//IDF_table contains all the words in the collection along with their IDF values
	//gIDF_table contains the same words as IDF_table, but the values are global (the web)
	//no_gIDF_words contains the words that are present in the positive collection, but not in the global IDF table.
	public function create_idf_tables(){
		echo "Creating local IDF file...\n";
		$IDF = array();
		$gIDF = array();
		$docCount = 0;
		$TotDocs = 0;
		
		//Count number of documents
		$dirIt = new DirectoryIterator($this->path."/pages/");
		foreach($dirIt as $file){
			if($file->isDot()){
				continue;
			} else {
				$docCount++;
			}
		}
		
		//Count number of documents each word appears in
		for($i = 1; $i <= $docCount; $i++){
			$page = new Page($this->path."/pages/$i.txt");
			$page->remove_stop_words(true);
			//No stemming, because global IDF table contains un-stemmed terms
			//$page->bagOfWords = $page->stem_words();
			
			$sumTerms = $page->sum_terms();
			foreach($page->bagOfWords as $word => $count){
				$word = trim($word);
				if(array_key_exists($word,$IDF)){
					$IDF[$word] += 1;
				} else {
					$IDF[$word] = 1;
				}
			}
			$TotDocs++;
			echo "Nbr of docs done so far: $TotDocs              \r";
		}
		
		echo "\nPrinting IDF table... ";
		//Sort (on IDF value) and save to file, the IDF table for the collection
		//Filename: IDF_table.txt
		arsort($IDF);
		$denom = log($docCount,10);
		$out = fopen($this->path."/svm/IDF_table.txt","w");
		
		//Calculate IDF for each word
		foreach($IDF as $word => $count){
			fwrite($out,(log($docCount/$count,10)/$denom)." $word\n");
		}
		fclose($out);
		echo "Done!\n";
		
		echo "Creating gIDF table... ";
		//Save global IDF values for this collection on file.
		//Words, from the collection, that are not present in the gIDF 
		//will receive the highest gIDF value: 0.9346501446 and will also
		//be saved in a separate file: no_gIDF_words.txt
		$h = fopen("includes/idf_az.txt","r");
		if($h === false)
			trigger_error("Could not open idf_az.txt", E_USER_ERROR);
		while($line = fgets($h)){
			$pair = explode(" ",$line); //0: val, 1: word
			$word = trim($pair[1]);
			$val = trim($pair[0]);
			if(array_key_exists($word, $IDF)){
				$gIDF[$word] = $val;
			}
		}
		fclose($h);
		echo "Done!\n";
		
		echo "Printing gIDF table...";
		//Actual printing of the gIDF table
		$out = fopen($this->path."/svm/gIDF_table.txt","w");
		$noIDF = fopen($this->path."/svm/no_gIDF_words.txt","w");
		foreach($IDF as $word => $count){
			if(array_key_exists($word,$gIDF)){
				fwrite($out,$gIDF[$word]." $word\n");
			} else {
				fwrite($out,"0.9346501446 $word\n");
				fwrite($noIDF,(log($docCount/$count,10)/$denom)." $word\n");
			}
		}
		fclose($out);
		fclose($noIDF);
		
		echo "Done!\n";
	}
	
	//Creates the positive training data for the collection.
	//Each row starts with the class lable (1 for positive) followed by the weights
	//given by the formula: (gIDF^2*TF)/IDF
	//$ceiling specifies the maximum frequency of any unique term: if it is set to 15 then no term will have a TF higher than 15.
	//$extended can be used as a postfix label so that the same method can be used to create the training file using the extended 
	//vocabulary (see the report for a description of the theory). The postfix "_extended" is used in the thesis.
	public function create_pos_training_data($ceiling,$extended = ""){
		echo "Creating training file...\n";

		$IDF = $this->table_to_array($this->path."/svm/IDF_table".$extended.".txt");
		$gIDF = $this->table_to_array($this->path."/svm/gIDF_table".$extended.".txt");
				
		$collectionSize = 0;
		$dirIt = new DirectoryIterator($this->path."/pages/");
		foreach($dirIt as $file){
			if($file->isDot())
				continue;
			$collectionSize++;
		}
		if($ceiling < 0)
			$filePath = $this->path."/svm/pos_train_max".$extended.".txt";
		else
			$filePath = $this->path."/svm/pos_train_".$ceiling.$extended.".txt";
			
		$out = fopen($filePath,"w");
		for($i = 1; $i <= $collectionSize; $i++){
			//Calculate alphas and save in file
			$page = new Page($this->path."/pages/$i.txt",$ceiling);
			$page->remove_stop_words(true);
			$sumTerms = $page->sum_terms();
			$vector_str = "1 ";
			$term_counter = 0;
			foreach($IDF as $word => $val){
				$term_counter++;
				if(isset($page->bagOfWords[$word])){
					$tfval = $page->bagOfWords[$word]/$sumTerms;
					$idfval = $val;
					$gidfval = $gIDF[$word];
					$weight = ($tfval*$gidfval*$gidfval)/$idfval;
					$vector_str .= $term_counter.":".$weight." ";
				}
			}
			fwrite($out, trim($vector_str));
			fwrite($out, "\n");
			echo "Docs done: $i                      \r";
		}
		fclose($out);
		$this->scale($filePath);
		echo "\nDone!\n";
		
	}
	
	//Creates the weights-file used for testing (using the manually categorized collection)
	public function create_testing_data($collection,$label,$ceiling,$extended = ""){ # = _extended for the extended version
		echo "Creating test file...\n";

		$IDF = $this->table_to_array($this->path."/svm/IDF_table".$extended.".txt");
		$gIDF = $this->table_to_array($this->path."/svm/gIDF_table".$extended.".txt");
		
		//If there is no ceiling; give the file the postfix "_max" otherwise the value of the ceiling
		if($ceiling < 0)
			$filePath = $this->path."/svm/testfile_".$collection."_max".$extended.".txt";
		else
			$filePath = $this->path."/svm/testfile_".$collection."_".$ceiling.$extended.".txt";
		
		$out = fopen($filePath,"w");
		$collectionSize = 0;
		$dirIt = new DirectoryIterator($this->path."/$collection/");
		foreach($dirIt as $file){
			if($file->isDot())
				continue;
			$collectionSize++;
		}
		
		for($i = 1; $i <= $collectionSize; $i++){
			//Calculate alphas and save in file
			$page = new Page($this->path."/$collection/$i.txt",$ceiling);
			$page->remove_stop_words(true);
			$sumTerms = $page->sum_terms();
			$vector_str = "$label ";
			$term_counter = 0;
			foreach($IDF as $word => $val){
				$term_counter++;
				if(isset($page->bagOfWords[$word])){
					$tfval = $page->bagOfWords[$word]/$sumTerms;
					$idfval = $val;
					$gidfval = $gIDF[$word];
					$weight = ($tfval*$gidfval*$gidfval)/$idfval;
					$vector_str .= $term_counter.":".$weight." ";
				}
			}
			fwrite($out, trim($vector_str));
			fwrite($out, "\n");
			echo "[$collection]: $i                      \r";
		}
		fclose($out);
		echo "\nDone!\n";
		$this->scale($filePath);
	}
	
	//Scale weight vectors so that all values fall between 0 and 1
	//$filePath is the file containing the weight vectors where each row is one vector 
	//This is mostly used internally, so it never has to be called explicitly but can be used seperate as well
	public function scale($filePath){
		echo "Looking for max...\n";
		$max = 0;
		$in = fopen($filePath,"r");
		$row = 0;
		while($line = fgets($in)){
			$line = strstr($line," ");
			if(empty($line))
				continue;
			$pairs = explode(" ",trim($line));
			foreach($pairs as $pair){
				$temp = explode(":",$pair);
				if($temp[1]>$max)
					$max = $temp[1];
			}
			$row++;
			echo "Row: $row, MAX = $max      \r";
		}
		echo "\nDone!\n";
		
		echo "Scaling...\n";
		rewind($in);
		$out = fopen($filePath.".scaled", "w");
		$row = 0;
		while($line = fgets($in)){
			$label = strstr($line," ",true);
			$line = strstr($line," ");
			if(empty($line))
				continue;
			fwrite($out,"$label");
			$pairs = explode(" ",trim($line));
			foreach($pairs as $pair){
				$temp = explode(":",$pair);
				$temp[1] /= $max;
				$temp[1] = round($temp[1],10);
				fwrite($out," ".$temp[0].":".$temp[1]);
			}
			fwrite($out,"\n");
			$row++;
			echo "Row: $row       \r";
		}
		fclose($out);
		fclose($in);
		echo "\nDone!\n";
	}
	
	//Iverts all weights in a training file.
	//$ceiling and $extended need to be specifie so that the right file can be found
	//see the other methods for filename standards 
	public function invert($ceiling,$extended = ""){
		echo "Inverting ".$this->name." pos_train_".$ceiling.$extended.".txt.scaled\n";
		if(!file_exists($this->path."/svm/pos_train_".$ceiling.$extended.".txt.scaled"))
			trigger_error("Could not find scaled training file for TF-ceiling $ceiling\n", E_USER_ERROR);
		
		$in = fopen($this->path."/svm/pos_train_".$ceiling.$extended.".txt.scaled", "r");
		$out = fopen($this->path."/svm/pos_train_".$ceiling."_inv".$extended.".txt.scaled", "w");
		
		$row = 0;
		while($line = fgets($in)){
			$label = trim(strstr($line," ",true));
			$line = trim(strstr($line," "));
			if(empty($line))
				continue;
			
			fwrite($out,"$label ");
			$pairs = explode(" ",trim($line));
			foreach($pairs as $pair){
				$temp = explode(":",$pair);
				$temp[1] = 1-$temp[1];
				fwrite($out," ".$temp[0].":".$temp[1]);
			}
			fwrite($out,"\n");
			$row++;
			echo "Row: $row       \r";
		}
		fclose($out);
		fclose($in);
		echo "\nDone!\n";
	}
	
	//Creates a file containing all the weights from the documents from random/pages.
	//Will be used to draw weights from when creating negative examples with random weights.
	//(Based on the local feature set)
	public function create_weights_from_rnd($ceiling){
		echo "Creating file with weights from random pages...\n";

		$path = "../results/random/pages/";
		$weights = array();
		$IDF = $this->table_to_array($this->path."/svm/IDF_table.txt");
		$gIDF = $this->table_to_array($this->path."/svm/gIDF_table.txt");
				
		$collectionSize = 0;
		$dirIt = new DirectoryIterator($path);
		foreach($dirIt as $file){
			if($file->isDot())
				continue;
			$collectionSize++;
		}
		for($i = 1; $i <= $collectionSize; $i++){
			//Calculate alphas and save in file
			$page = new Page($path."$i.txt",$ceiling);
			$page->remove_stop_words(true);
			$sumTerms = $page->sum_terms();
			$term_counter = 0;
			foreach($IDF as $word => $val){
				$term_counter++;
				if(isset($page->bagOfWords[$word])){
					$tfval = $page->bagOfWords[$word]/$sumTerms;
					$idfval = $val;
					$gidfval = $gIDF[$word];
					$weights[] = round((($tfval*$gidfval*$gidfval)/$idfval),10);
				}
			}
			echo "Docs done: $i                      \r";
		}
		echo "Looking for max...\n";
		$max = 0;
		foreach($weights as $index => $weight){
			if($weight > $max){
				$max = $weight;
			}
		}
		
		echo "Scaling (max = $max)...\n";
		foreach($weights as $index => $weight){
			$weights[$index] = $weight/$max;
		}
		file_put_contents($this->path."/svm/rnd_weights_".$ceiling."_scaled.txt",implode(" ",$weights));
		echo "\nDone!\n";
	}
	
	//Creates negative training vectors based on weights from the random documents
	//found in the file rnd_weights_15_scaled.txt, created by the function <create_weights_from_rnd()>
	//All weights are scaled in the resulting file!
	//See section 4.3.4 in the report
	public function create_neg_rndp($ceiling){	
		$posFile = $this->path."/svm/pos_train_".$ceiling.".txt.scaled";
		if(!file_exists($posFile))
			trigger_error("No scaled training file found for ceiling $ceiling\n", E_USER_ERROR);
		
		if(!file_exists($this->path."/svm/rnd_weights_".$ceiling."_scaled.txt"))
			trigger_error("No scaled weights file found for negative examples ($ceiling)\n", E_USER_ERROR);
		echo "Creating weights from random pages\n";
		
		#OBS! indices range from 0 to size, but there is no feature 0. Add one when printing
		$indices = array_keys(explode("\n",trim(file_get_contents($this->path."/svm/IDF_table.txt"))));
		$lengths = explode(" ",trim(file_get_contents("../results/complete_doc_lengths_".$ceiling.".txt")));
		$weights = explode(" ",trim(file_get_contents($this->path."/svm/rnd_weights_".$ceiling."_scaled.txt")));
		$sz_weights = count($weights)-1;
		$size = count($lengths);
		$out = fopen($this->path."/svm/train_rnd_from_rndp_complete_".$ceiling.".txt","w");
		fwrite($out,file_get_contents($posFile));
		for($i = 0; $i<5000; $i++){
			echo "Printing row $i         \r";
			fwrite($out,"-1");
			$wc = $lengths[mt_rand(0,$size-1)]; #wc = word count
			shuffle($indices);
			$pairs = array();
			for($c = 0; $c < $wc; $c++){
				$weight = $weights[mt_rand(0,$sz_weights)];
				#OBS! Add one to value from indices as there is no feature number 0
				$pairs[1+$indices[$c]] = $weight;
			}
			ksort($pairs);
			foreach($pairs as $index => $weight){
				fwrite($out," ".$index.":".$weight);
			}
			fwrite($out,"\n");
		}
		fclose($out);
		echo "\nDone!\n";
	}
	
	//Creates 5000 artificial negative examples based on uniform distribution
	//of weights and appends the positive examples to create the "complete" training file containing both positive examples
	//and artificial negative based on a uniform distribution of weights. 
	//$ceiling specifies which positive examples to use (distinguished by the TF ceiling)
	//All random weights are between 0 and 1!
	//See section 4.3.1 in the report for details
	public function create_neg_uniform($ceiling){
		$posFile = $this->path."/svm/pos_train_".$ceiling.".txt.scaled";
		if(!file_exists($posFile))
			trigger_error("No scaled training file found for ceiling $ceiling\n", E_USER_ERROR);
		
		echo "Creating random weights train file\n";
		#OBS! indices range from 0 to size, but there is no feature 0. Add one when printing
		$indices = array_keys(explode("\n",trim(file_get_contents($this->path."/svm/IDF_table.txt"))));
		$lengths = explode(" ",trim(file_get_contents("../results/complete_doc_lengths_".$ceiling.".txt")));
		$size = count($lengths);
		$out = fopen($this->path."/svm/train_rnd_uniform_complete_".$ceiling.".txt","w");
		fwrite($out,file_get_contents($posFile));
		$max = mt_getrandmax();
		for($i = 0; $i<5000; $i++){
			echo "Printing row $i         \r";
			fwrite($out,"-1");
			$wc = $lengths[mt_rand(0,$size-1)];
			shuffle($indices);
			$pairs = array();
			for($c = 0; $c < $wc; $c++){
				$weight = round(mt_rand(0,$max)/$max, 10);
				#OBS! Add one to value from indices as there is no feature number 0
				$pairs[1+$indices[$c]] = $weight;
			}
			ksort($pairs);
			foreach($pairs as $index => $weight){
				fwrite($out," ".$index.":".$weight);
			}
			fwrite($out,"\n");
		}
		fclose($out);
		echo "\nDone!\n";
	}
	
	//Creates a file of weight probabilities for the positive examples speficied by the $ceiling and $extended parameters
	//(positive weight-vector files are named after what ceiling was used and whether or not the extended vocabulary was used.
	//Needed for creating randomized negative examples
	function create_weight_probs($ceiling,$extended=""){
		echo "Creating weight probability file...\n";
		$in = fopen($this->path."/svm/pos_train_".$ceiling."_inv".$extended.".txt.scaled","r");
		$out = fopen($this->path."/svm/weight_probs_".$ceiling.$extended.".txt","w");
		$weights = array();
		$row = 0;
		while($line = fgets($in)){
			$label = trim(strstr($line," "),true);
			$line = trim(strstr($line," "));
			if(empty($line))
				continue;
			
			$pairs = explode(" ",trim($line));
			foreach($pairs as $pair){
				$temp = explode(":",$pair);
				$weights[] = $temp[1];
			}
			$row++;
			echo "Row: $row       \r";
		}
		echo "\nSorting...\n";
		sort($weights);
		echo "Counting probabilities...\n";
		$weights = array_count_values($weights);
		echo "Saving to file...\n";
		foreach($weights as $weight => $count){
			fwrite($out,$count." ".$weight."\n");
		}
		fclose($in);
		fclose($out);
		echo "Done!\n";
	}
	
	//Probablity table of the IDF-table
	//Needed for creating randomized negative examples
	public function create_idf_probs($ceiling,$extended = ""){
		echo "Createing idf probability table...\n";
		$weights = $this->table_to_array($this->path."/svm/IDF_table".$extended.".txt");
		echo "Counting values...\n";
		$weights = array_count_values($weights);
		echo "Sorting...\n";
		ksort($weights);
		echo "Saving to file...\n";
		$out = fopen($this->path."/svm/idf_probs_".$ceiling.$extended.".txt","w");
		foreach($weights as $weight => $count){
			fwrite($out,$count." ".$weight."\n");
		}
		fclose($out);
	}
	
	//Really just used once to complie a idf-probability table over all the categories. 
	//NOTE! The categories used in this thesis are hard-coded in the method. Not very pretty, but refactor if needed.
	//Needed for creating randomized negative examples
	public function collect_idf_probs($ceiling,$extended =""){
		echo "Creating complete idf table...\n";
		$complete = array();
		$names = array("christianity","cooking","martialarts","random");
		foreach($names as $name){
			echo "Compiling $name...\n";
			$arr = $this->table_to_array("../results/$name/svm/idf_probs_".$ceiling.$extended.".txt");
			foreach($arr as $val => $count){
				if(isset($complete[$val]))
					$complete[$val] += $count;
				else
					$complete[$val] = $count;
			}
		}
		echo "Sorting...\n";
		ksort($complete);
		echo "Saving...\n";
		$out = fopen("../results/complete_idf_probs_".$ceiling.$extended.".txt","w");
		foreach($complete as $weight => $count){
			fwrite($out,$count." ".$weight."\n");
		}
		fclose($out);		
	}
	
	//Create negative examples based on the inverse of the positive examples.
	//See section 4.3.2 for an explanation
	public function neg_inverse($ceiling,$extended = ""){
		$IDF = $this->table_to_array("../results/complete_idf_probs_".$ceiling.$extended.".txt"); #IDF values are randomly picked from this vector
		$sum_idf = array_sum($IDF); #Needed for randomly selecting IDF values
		
		$vectors = array(); #Will contain all vectors (excluding the label)
		$doc_lengts = array();
		
		$gIDF = $this->table_to_array($this->path."/svm/gIDF_table".$extended.".txt"); 
		$tot_terms = count($gIDF); #Only needed when randomly selecting terms
		$words = array_keys($gIDF); #Makes it possible to index words by their position in the vector
		$term_indices = array_keys($words); #Used to index terms and when writing vectors ($index:$weigth)
		$vector_indices = array();
		for($i = 0; $i < 5000; $i++){
			$vector_indices[$i] = $i;
		}
		
		// shuffle($indices); #If randomization of terms is needed (when not using all terms)
		$inverse = $this->table_to_array($this->path."/svm/weight_probs_".$ceiling.$extended.".txt"); #The inverse weights by order and distribution
		$inv_count = count($inverse);
		$sum_inv = array_sum($inverse); # Needed for random selection of inverse weight
		
		//Expand the probability table so that if valux X occurs twice in $inverse, then the same value will be in two places in the array
		//eg new_inv[i] = X and new_inv[i+1] = X. Because the random picking will be done by generating a number between 0 and (number of entries in the array).
		//The weights_probs_ file that is read above contains lines like: "weight count" where count determines how many slots in the array that weight gets
		$new_inv = array();
		$counter = 0;
		foreach($inverse as $val => $ind){
			for($i = 1; $i <= $ind; $i++){
				$new_inv[$counter+$i] = $val;
			}
			$counter += $ind;
		}
		unset($inverse);
		
		//Same expansion as above
		$counter = 0;
		$new_IDF = array();
		foreach($IDF as $val => $ind){
			for($i = 1; $i <= $ind; $i++){
				$new_IDF[$counter+$i] = $val;
			}
			$counter += $ind;
		}
		unset($IDF);
		
		//The actual weights are calculated. Read section 4.3.2 in the report for an explanation
		foreach($term_indices as $term_nbr){
			$term = $words[$term_nbr];
			$idf = $new_IDF[mt_rand(1,$sum_idf)];
			
			$df = round(5000/exp($idf*log(5000)),0);
			echo "Doing term $term_nbr (out of $tot_terms) [df = $df]\n";
			$gidf = $gIDF[$term];
			shuffle($vector_indices);
			
			//Random alpha, calculate TF
			for($i = 0; $i < $df; $i++){
				$inv = $new_inv[mt_rand(1,$sum_inv)];
				
				$tf = round(($inv*$idf)/($gidf*$gidf),0);
				if($tf > 15){
					echo "\n-------------------\nFOUND TF > 15! DF = $df, alpha = $inv\n-------------------\n";
					return;
				}
				if(isset($vectors[$vector_indices[$i]]))
					$vectors[$vector_indices[$i]] .= " ".($term_nbr+1).":$inv";
				else 
					$vectors[$vector_indices[$i]] = " ".($term_nbr+1).":$inv";
					
				if(isset($doc_lengts[$vector_indices[$i]]))
					$doc_lengts[$vector_indices[$i]] += $tf;
				else 
					$doc_lengts[$vector_indices[$i]] = $tf;
			}
		}
		$out = fopen($this->path."/svm/train_inv_complete_".$ceiling.$extended.".txt","w");
		$posFile = $this->path."/svm/pos_train_".$ceiling.$extended.".txt.scaled";
		fwrite($out,file_get_contents($posFile));
		foreach($vectors as $vector){
			fwrite($out,"-1".$vector."\n");
		}
		fclose($out);
		
		$out = fopen($this->path."/svm/doc_lengths_inv_only_neg_".$ceiling.$extended.".txt","w");
		foreach($doc_lengts as $doc_length){
			fwrite($out,$doc_length."\n");
		}
		fclose($out);
	}
	
	//Creates the extended vocabularies (the IDF tables are used as vocabularies as they contain all the terms in the respective collection)
	//By settting the paratmeter $extended to "_extended" in the methods were it is available, these IDF tables are used instead
	public function create_extended_idf(){
		echo "Creating extended idf tables...\n";
		$extended = $this->table_to_array("includes/idf_az_100k_most_common.txt");
		$IDF = $this->table_to_array($this->path."/svm/IDF_table.txt");
		$gIDF = $this->table_to_array($this->path."/svm/gIDF_table.txt");
		$outIDF = fopen($this->path."/svm/IDF_table_extended.txt","w");
		$outgIDF = fopen($this->path."/svm/gIDF_table_extended.txt","w");
		fwrite($outIDF,trim(file_get_contents($this->path."/svm/IDF_table.txt")));
		fwrite($outgIDF,trim(file_get_contents($this->path."/svm/gIDF_table.txt")));
		$counter = 0;
		foreach($extended as $word => $val){
			$counter++;
			echo "Checking $counter          \r";
			if(!isset($IDF[$word])){
				fwrite($outIDF,$val." ".$word."\n");
			}
			if(!isset($gIDF[$word])){
				fwrite($outgIDF,$val." ".$word."\n");
			}
		}
		fclose($outIDF);
		fclose($outgIDF);
		echo "\nDone!\n";
	}
	
	//This method is used to create negative examples (in the form of weight vectors) based on pages from another category.
	//It was mostly used to create negative examples directly from random pages. The category used then was named "random" 
	//which contained 5000 radnom documents from DMOZ
	//See section 4.3.5 in the report
	public function create_neg_from_other_cat($ceiling,$otherName){	
		
		$posFile = $this->path."/svm/pos_train_".$ceiling.".txt.scaled";
		$negFile = $this->path."/svm/train_neg_from_".$otherName."_".$ceiling.".txt";
		if(!file_exists($posFile))
			trigger_error("No scaled training file found for ceiling $ceiling\n", E_USER_ERROR);
		
		// if(!file_exists("../results/$otherName/svm/pos_train_".$ceiling.".txt.scaled"))
			// trigger_error("No pos_train_$ceiling file found for $otherName\n", E_USER_ERROR);
		// $in = fopen("../results/$otherName/svm/pos_train_".$ceiling.".txt.scaled","r");
		
		echo "[".$this->name."] Creating negative weights from $otherName pages\n";
		$out = fopen($negFile,"w");
		// fwrite($out,file_get_contents($posFile));
		
		############
		$path = "../results/$otherName/pages/";
		$weights = array();
		$IDF = $this->table_to_array($this->path."/svm/IDF_table.txt");
		$gIDF = $this->table_to_array($this->path."/svm/gIDF_table.txt");
				
		$collectionSize = 0;
		$dirIt = new DirectoryIterator($path);
		foreach($dirIt as $file){
			if($file->isDot())
				continue;
			$collectionSize++;
		}
		for($i = 1; $i <= $collectionSize; $i++){
			//Calculate alphas and save in file
			$page = new Page($path."$i.txt",$ceiling);
			$page->remove_stop_words(true);
			$sumTerms = $page->sum_terms();
			fwrite($out, "-1");
			$term_counter = 0;
			foreach($IDF as $word => $val){
				$term_counter++;
				if(isset($page->bagOfWords[$word])){
					$tfval = $page->bagOfWords[$word]/$sumTerms;
					$idfval = $val;
					$gidfval = $gIDF[$word];
					$weight = round((($tfval*$gidfval*$gidfval)/$idfval),10);
					fwrite($out," ".$term_counter.":".$weight);
				}
			}
			// fwrite($out, trim($vector_str));
			fwrite($out, "\n");
			echo "Docs done: $i                      \r";
		}
		fclose($out);
		$this->scale($this->path."/svm/train_neg_from_".$otherName."_".$ceiling.".txt");
		$out = fopen($this->path."/svm/train_neg_from_".$otherName."_".$ceiling."_complete.txt","w");
		fwrite($out,file_get_contents($posFile));
		fwrite($out,file_get_contents($this->path."/svm/train_neg_from_".$otherName."_".$ceiling.".txt.scaled"));
		fclose($out);
		echo "\nDone!\n";
	}
	
}

####
# Preparation for extended versions
// $svm = new SVM("cooking");
// $svm->create_extended_idf();
// $svm->create_idf_probs(15,"_extended");

// $svm = new SVM("random");
// $svm->create_extended_idf();
// $svm->create_idf_probs(15,"_extended");

// $svm = new SVM("martialarts");
// $svm->create_extended_idf();
// $svm->create_idf_probs(15,"_extended");

// $svm->collect_idf_probs(15,"_extended");



// $svm = new SVM("christianity");
// $svm = new SVM("cooking");
// $svm = new SVM("martialarts");
// $svm = new SVM("random");
// $svm->create_pos_training_data(15);
// $svm->create_neg_uniform(15);

// $svm->create_weights_from_rnd(15); #Must preceed call to create_neg_rndp($ceiling).. but only once
// $svm->create_neg_rndp(15);

//$svm->create_testing_data("man_pos","1",15);
// $svm->create_testing_data("man_neg","-1",15);
// $svm->invert(15);
// $svm->create_weight_probs(15);
// $svm->create_idf_probs(15);
// $svm->neg_inverse(15);


#####
# Creation of extended training and test data
// $svm = new SVM("christianity");
// $svm = new SVM("cooking");
// $svm = new SVM("martialarts");
// $svm->create_pos_training_data(15,"_extended");
// $svm->create_testing_data("man_pos","1",15,"_extended");
// $svm->create_testing_data("man_neg","-1",15,"_extended");
// $svm->invert(15,"_extended");
// $svm->create_weight_probs(15,"_extended");
// $svm->neg_inverse(15,"_extended");

######
# Extra: use random pages as negative directly
// $svm = new SVM("christianity");
// $svm->create_neg_from_other_cat(15,"random");
// $svm = new SVM("cooking");
// $svm->create_neg_from_other_cat(15,"random");
// $svm = new SVM("martialarts");
// $svm->create_neg_from_other_cat(15,"random");


?>