<?php
ini_set("memory_limit","1000M");
require("includes/Page.php");

$maPath = "/Sports/Martial_Arts/";
$coPath = "/Home/Cooking/";
$chPath = "/Society/Religion_and_Spirituality/Christianity/";

class Crawler{
	//command constants
	const BAD_URL = 0;
	const REL_URL = 1;
	const ABS_URL = 2;

	private $ctx;
	private $category_path = "";
	private $name = "";
	private $resPath = "";
	
	function __construct($DMOZcategory, $name){
		$this->category_path = $DMOZcategory;
		$this->name = $name;
		$this->resPath = "../results/$name/";
		$this->ctx = stream_context_create(array(
			'http' => array(
			'timeout' => 5,
			'user_agent' => "Thesis_php_crawler"
			)
		));
		if(!file_exists($this->resPath)){
			if(!mkdir($this->resPath))
				trigger_error("Could not create directory \"$name\"", E_USER_ERROR);
		}
	}
	
	/*
	 * The function crawls the dmoz website, collecting links from the subcategories
	 * $sub   - the (sub)category to traverse
	 * $urls  - a (pass by reference) array where all the external links belonging to the $sub category will be added at the end
	 *          to any of the subcategories (or head category)
	 * RETURN - number of external links belonging to the category $sub
	 */
	private function dmoz_extract_urls($sub, &$urls, $maxNbr=-1){
		$counter = 0;
		$page = file_get_contents("http://www.dmoz.org".$sub);
		preg_match_all("/(<ul class=\"directory-url\"[^>]*>)(.*?)(<\/ul>)/ms", $page, $matches, PREG_SET_ORDER);
		foreach($matches as $val){
			preg_match_all("/<a href=\"(.*?)\">(.*?)<\/a>/", $val[2], $links, PREG_SET_ORDER);
			foreach($links as $link){
				$link[1] = rtrim($link[1],"/"); #http://www.site.com/ is same as http://www.site.com
				if(!in_array($link[1], $urls)){
					$urls[] = $link[1];
					$counter++;
				}
				if($counter == $maxNbr) //if maxNbr = -1, no restriction applies
					return $counter;
			}
		}
		return $counter;
	}
	
	//Adds new subcategories in the (passed by reference) array $subs
	//Returns number of subsections added
	private function dmoz_extract_subs($base, $sub, &$subs){
		$base = addcslashes($base, "/");
		$counter = 0;
		$page = file_get_contents("http://www.dmoz.org".$sub);
		preg_match_all("/(<ul class=\"directory dir-col\"[^>]*>)(.*?)(<\/ul>)/ms", $page, $matches, PREG_SET_ORDER);
		foreach($matches as $val){
			preg_match_all("/<a href=\"(".$base.".*?)\">(.*?)<\/a>/", $val[2], $links, PREG_SET_ORDER);
			foreach($links as $link){
				if(!in_array($link,$subs)){
					$subs[] = $link[1];
					$counter++;
				}
			}
		}
		return $counter;
	}
	
		/*
	 * $urlList - array of urls
	 * RETURN   - array or urls that point to the index page
	              e.g. of the form http://www.somesite.com
				  but not http://www.somesite.com/somepage.html
	 */
	private function extract_non_specific_urls($urlList){
		$exp = "/http:\/\/[^\/]+\/?$/";
		$indices = preg_grep($exp, $urlList);
		return array_values($indices);
	}

	/*
	 * $url      - the url to the page you want to download
	 * $filename - name of the file to save the page to (will overwrite)
	 * RETURN    - void
	 */
	private function saveWebPage($url, $filename){
		$page = file_get_contents($url);
		
		$handle = fopen($filename, 'w');
		fwrite($handle, $page);
		fclose($handle);
	}

	/*
	 * $aTagContents - string of the contents between <a and /> (see return of extrac_a_tag_contents)
	 * RETURN        - the actual url given by the href attribute or empty string if no href is present.
	 */
	private function extract_href($aTagContents){
		$aTagContents = trim($aTagContents);
		$arr = preg_match_all("/href=\"(.*?)\"/",$aTagContents,$matches,PREG_PATTERN_ORDER);
		if(isset($matches[1][0]))
			return $matches[1][0];
		else
			return "";
	}
	
	/*
	 * $HTMLpage - string with the contents of a html-page (use file_get_contents or some such)
	 * RETURN    - array of strings. Each string is the contents between <a and /> 
	 *             e.g. <a href="/page" style="color: pink;"/> => ' href="/page" style="color: pink;"'
	 */
	private function extract_a_tag_contents($HTMLpage){
		$arr = preg_match_all("/<a(.*?)>/",$HTMLpage,$matches,PREG_PATTERN_ORDER);
		return $matches[1];
	}
	
	/*
	 * Since an absolute link can be written as http://www.domain.xx/page.php or http://domain.xx/page.php
	 * both are needed in is_internal (some sites cotain link of both forms. 
	 * $domain - string containing one of the forms
	 * RETURN  - array of two elements, the first is the short form, the second is the "www." form
	 */
	private function make_domain_alts($domain){
		$domain = trim($domain,"/");
		$result = array();
		$alternative = $domain;
		if(preg_match("/http:\/\/www./",$domain)){
			$alternative = substr($domain,0,7).substr($domain, 11, strlen($domain));
		} else {
			$alternative = substr($domain,0,7)."www.".substr($domain, 7, strlen($domain));
		}
		$result[0] = $domain;
		$result[1] = $alternative;
		return $result;
	}

	/*
	 * Test to see if a link points inside the domain or not
	 * $domain - http://www.domain.xx or http://domain.xx
	 * $url    - the link to be tested. If it is relative but does not
	 *           start with a /, it will be added (note pass by reference)
	 * RETURN  - true or false
	 */
	private function is_internal($domain,$url){
		if(empty($url))
			return self::BAD_URL;
		else {
			$slashdom1 = addcslashes($domain[0], "/");
			$slashdom2 = addcslashes($domain[1], "/");
			if(!preg_match("/http[s]?:\/\//",$url) && ($url[0] != '#') && !preg_match("/^javascript/",$url)){
				return self::REL_URL;
			} else if(preg_match("/^$slashdom1|^$slashdom2/",$url)){
				//This is to exclude any subdomains e.g. site.com.ar
				$domainBits1 = explode("/",$domain[0]); //http://www.site.com => www.site.com at [2]
				$domainBits2 = explode("/",$domain[1]); //http://site.com => site.com at [2]
				$urlBits = explode("/",$url); //http://www.somesite.com.ar/blabla => www.somesite.com.ar at [2]
				if(strcasecmp($domainBits1[2],$urlBits[2])==0 || strcasecmp($domainBits2[2],$urlBits[2])==0)
					return self::ABS_URL;
				else 
					return self::BAD_URL;
			} else {
				return self::BAD_URL;
			}
		}	
	}
	
	private function make_abs_url($domain, $url){
		return rtrim($domain, "/")."/".trim($url, "/");
	}

	
	
	//Produces a file (subcategories.txt) containing all the subcategories under the main category
	public function get_subs(){
		echo "Extracting subs for ".$this->name."\n";
		$base = $this->category_path;
		//Arrays for storing the subsections
		$subs = array();
		
		//Start with the main section and init pointer
		$subs[] = $base;
		$subs_p = 0;
		$count = 1;

		//Go through each subsection and extract new subs
		while($subs_p < $count){
			$newSub = $subs[$subs_p];
			//$subs is passed as reference
			$count += $this->dmoz_extract_subs($base, $newSub, $subs);
			echo "   Visited $subs_p out of $count         \r";
			$subs_p++;
		}
		
		$subs = array_unique($subs);
		$sub_string = implode("\n",$subs);
		file_put_contents($this->resPath."subcategories.txt",$sub_string);
		echo "\nDone!\n-----------\n";
	}
	
	//Produces a file (mixed_urls.txt) containing all the external urls from all
	//the categories in the "subcategories.txt" file. Will most likely contain index pages
	//as well (www.domname.xx) [see next function]
	public function get_urls($maxUrlPerSub = -1){
		echo "Extracting URL's from category pages...\n";
		$subList = file_get_contents($this->resPath."subcategories.txt");
		if($subList === false)
			trigger_error("Couldn't find a subcategory list", E_USER_ERROR);
		$subs = explode("\n", $subList);
		$urls = array();
		$count = 0;
		$subCount = 1;
		$subSize = count($subs);
		foreach($subs as $sub){
			$count += $this->dmoz_extract_urls($sub, $urls, $maxUrlPerSub);
			echo "  Nbr of urls so far: $count, cateogries left: ".($subSize-$subCount)."         \r";			
			$subCount++;
		}
		$urls = array_unique($urls);
		$urlstring = implode("\n",$urls);
		file_put_contents($this->resPath."mixed_urls.txt",$urlstring);
		echo "\nDone!\n-----------\n";
	}
	
	
	//Produces a file (good_urls.txt) containing all URLs from all the sub categories along with all the first-level URLs from index pages
	//(since an index pages seldom contributs much to the classification task). 
	//The order of the URLs are randomized before being save to the file.
	//Also saves a list of bad URLs (bad_urls.txt)
	public function create_url_list(){
		echo "Start of create_url_list()\n";
		
		//Counting urls
		$in = fopen($this->resPath."mixed_urls.txt","r");
		$size = 0;
		while(fgets($in)){
			$size++;
		}
		fclose($in);
		
		$urlList = fopen($this->resPath."mixed_urls.txt","r");
		if($urlList === false){
			trigger_error("Could not open URL list", E_USER_ERROR);
		}
		$good_url_list = array();
		$bad_url_list = array();
		$counter = 1;
		while($url = trim(fgets($urlList))){
			echo "  Done $counter URLs out of $size          \r";
			if(preg_match("/http:\/\/[^\/]+\/?$/",$url)){		
				######
				## This section extracts all the first level links from the index page
				$page = @file_get_contents($url,0,$this->ctx);
				if($page === false){
					$bad_url_list[] = $url;
					continue;
				}
				//Get all the href tags
				$aContents = $this->extract_a_tag_contents($page);
				//Extract the actual urls (appart from other attributes)
				$hrefs = array();
				foreach($aContents as $aContent){
					$hrefs[] = $this->extract_href($aContent);
				}
				$domTest = $this->make_domain_alts($url);
				//Make the first url of the internal links the index page
				$good_url_list[] = $url;
				foreach($hrefs as $href){
					$href = trim($href,"/");
					$urlType = $this->is_internal($domTest,$href);
					if($urlType != $this::BAD_URL){
						if($urlType == $this::REL_URL){
							$href = $this->make_abs_url($url,$href);
						}
						$good_url_list[] = $href;
					} else {
						$bad_url_list[] = $href;
					}
				}
			} else {
				$good_url_list[] = $url;
			}
			$counter++;
		}
		fclose($urlList);
		
		echo "\nSaving results...\n";
		$good_url_list = array_unique($good_url_list);
		shuffle($good_url_list);
		$good_str = implode("\n",$good_url_list);
		
		$bad_url_list = array_unique($bad_url_list);
		$bad_str = implode("\n",$bad_url_list);
		
		if(file_put_contents($this->resPath."good_urls.txt", trim($good_str)))
			echo "Good urls saved!\n";
		else
			echo "Failed to save good urls!\n";
			
		if(file_put_contents($this->resPath."bad_urls.txt", trim($bad_str)))
			echo "Bad urls saved!\n";
		else
			echo "Failed to save bad urls!\n";
		
		echo "\nDone!\n-----------\n";
	}
	
	//Saves $wantedAmount of HTML-documents in the folder "pages", from the good_urls.txt list.
	//Only text files, with word bags >= 10 are saved.
	//Text files with word bag < 10 are saved in folder "small_pages" for evaluation
	public function crawl_and_save($wantedAmount,$urlList = "good_urls.txt"){
		echo "Saving documents from URL list...\n";
		if(!file_exists($this->resPath."pages")){
			if(!mkdir($this->resPath."pages"))
				trigger_error("Could not create directory \"pages\"", E_USER_ERROR);
		}
		
		if(!file_exists($this->resPath."small_pages")){
			if(!mkdir($this->resPath."small_pages"))
				trigger_error("Could not create directory \"small_pages\"", E_USER_ERROR);
		}
		
		$fp = fopen($this->resPath.$urlList, "r");

		$small_pages = array();
		$saved_urls = array();
		$nbr_of_saved = 0;
		$nbr_of_small = 0;
		
		while(($url = trim(fgets($fp))) && ($nbr_of_saved < $wantedAmount)){
			//Try to access the web page
			$ret = @file_get_contents($url,0,$this->ctx);
			//If it failed, save in bad_urls and continue with next url
			if($ret === false){
				echo "Failed to open $url\n";
				continue;
			}
			
			//Check content type. If no content type, skip the url
			//May contain several content-type entries, the last one 
			//is the one needed in that case.
			$ct = preg_grep("/^content-type/i",$http_response_header);
			$ct = array_values($ct);
			$ct_count = count($ct);
			if($ct_count == 1){
				$type = $ct[0];
			} else if($ct_count > 1){
				$type = $ct[$ct_count-1];
			} else {
				echo "Did not find content-type for $url\n";
				continue;
			}
			
			//Make sure it's a text file, otherwise skip url.
			if(preg_match("/text\//i",$type)){
				$ret = "<!--$url-->\n".$ret;
				//If the word bag is empty, there is no point in using the page
				$page = new Page($ret,-1,true);
				$page->remove_stop_words(true);
				if(count($page->bagOfWords) <= 10){
					$nbr_of_small++;
					$filename = $this->resPath."small_pages/".$nbr_of_small.".txt";
					if(file_put_contents($filename,$ret) === false){
						echo "Failed to save SMALL doc nbr $nbr_of_small\n";
						$nbr_of_small--;
						continue;
					} else {
						echo "Saved SMALL docs: $nbr_of_small ($url)\n";
					}
					$small_pages[] = $url;
					continue;
				}
				
				//Save file
				$nbr_of_saved++;
				$filename = $this->resPath."pages/".$nbr_of_saved.".txt";
				if(file_put_contents($filename,$ret) === false){
					echo "Failed to save doc nbr $nbr_of_saved\n";
					$nbr_of_saved--;
				} else {
					$saved_urls[] = $url;
					echo "Saved docs: $nbr_of_saved ($url)\n";
				}
			} else {
				echo "Not text file [$url]\n";
				continue; #URL does not point to a text file
			}
						
		}
		fclose($fp);
		file_put_contents($this->resPath."saved_urls.txt",implode("\n",$saved_urls));
		file_put_contents($this->resPath."small_pages_list.txt",implode("\n",$small_pages));
		echo "\nDone!\n-----------\n";
	}
	
	//Saves $wantedAmount of HTML-documents in the "manual" folder for manual categorization.
	//These come from the same "pool" of pages as the training ones and uses therefor the rest_urls.txt
	//file that contains the urls that were not used to fetch the training examples. That file is produced
	//by the create_rest_file() method in Utilities.php
	public function save_manual($wantedAmount){
		if(!file_exists($this->resPath."manual")){
			if(!mkdir($this->resPath."manual"))
				trigger_error("Could not create directory \"manual\"", E_USER_ERROR);
		}
		
		$urlList = explode("\n",trim(file_get_contents($this->resPath."rest_urls.txt")));
		$size = count($urlList);
		shuffle($urlList);
		
		$saved_urls = array();
		$nbr_of_saved = 0;
		
		for($i = 0; ($i < $size) && ($nbr_of_saved < $wantedAmount); $i++){
			$url = $urlList[$i];
			//Try to access the web page
			$ret = @file_get_contents($url,0,$this->ctx);
			//If it failed, save in bad_urls and continue with next url
			if($ret === false){
				echo "Failed to open $url\n";
				continue;
			}
			
			//Check content type. If no content type, skip the url
			//May contain several content-type entries, the last one 
			//is the one needed in that case.
			$ct = preg_grep("/^content-type/i",$http_response_header);
			$ct = array_values($ct);
			$ct_count = count($ct);
			if($ct_count == 1){
				$type = $ct[0];
			} else if($ct_count > 1){
				$type = $ct[$ct_count-1];
			} else {
				echo "Did not find content-type for $url\n";
				continue;
			}
			
			//Make sure it's a text file, otherwise skip url.
			if(preg_match("/text\//i",$type)){
				$ret = "<!--$url-->\n".$ret;
				//If the word bag is empty, there is no point in using the page
				$page = new Page($ret,-1,true);
				$page->remove_stop_words(true);
				if(count($page->bagOfWords) <= 10){
					continue;
				}
				
				//Save file
				$nbr_of_saved++;
				$filename = $this->resPath."manual/".$nbr_of_saved.".html";
				if(file_put_contents($filename,$ret) === false){
					echo "Failed to save doc nbr $nbr_of_saved\n";
					$nbr_of_saved--;
				} else {
					echo "Saved docs: $nbr_of_saved (".$this->name.")\n";
					$saved_urls[] = $url;
				}
			} else {
				echo "Not text file [$url]\n";
				continue; #URL does not point to a text file
			}
						
		}
		$rest = array_diff($urlList, $saved_urls);
		file_put_contents($this->resPath."rest_urls.txt",implode("\n",$rest));
		file_put_contents($this->resPath."saved_manual.txt",implode("\n",$saved_urls));		
		echo "\nDone!\n";
	}
	
	//Saves $wantedAmount number of pages from the $urllist
	//This is basically a copy of crawl_and_save (above) but is only used
	//for the collection of random pages. Copy-paste programming FTW!
	//Feel free to refactor this appropriately
	public function save_random_docs($urllist,$wantedAmount){
		if(!file_exists($this->resPath."pages")){
			if(!mkdir($this->resPath."pages"))
				trigger_error("Could not create directory \"pages\"", E_USER_ERROR);
		}
		
		if(!file_exists($this->resPath."small_pages")){
			if(!mkdir($this->resPath."small_pages"))
				trigger_error("Could not create directory \"small_pages\"", E_USER_ERROR);
		}
		
		$urls = explode("\n",trim(file_get_contents($urllist)));
		shuffle($urls);
		$small_pages = array();
		$saved_pages = array();
		$nbr_of_small = 0;
		$nbr_of_saved = 0;
		$size = count($urls);
		for($i=0; ($i<$size) && ($nbr_of_saved<$wantedAmount); $i++){
			$url = $urls[$i];
			
			//Try to access the web page
			$ret = @file_get_contents($url,0,$this->ctx);
			//If it failed, save in bad_urls and continue with next url
			if($ret === false){
				echo "Failed to open $url\n";
				continue;
			}
			
			//Check content type. If no content type, skip the url
			//May contain several content-type entries, the last one 
			//is the one needed in that case.
			$ct = preg_grep("/^content-type/i",$http_response_header);
			$ct = array_values($ct);
			$ct_count = count($ct);
			if($ct_count == 1){
				$type = $ct[0];
			} else if($ct_count > 1){
				$type = $ct[$ct_count-1];
			} else {
				echo "Did not find content-type for $url\n";
				continue;
			}
			
			//Make sure it's a text file, otherwise skip url.
			if(preg_match("/text\//i",$type)){
				$ret = "<!--$url-->\n".$ret;
				//If the word bag is smalle than 10, there is no point in using the page
				$page = new Page($ret,-1,true);
				$page->remove_stop_words(true);
				if(count($page->bagOfWords) <= 10){
					$nbr_of_small++;
					$filename = $this->resPath."small_pages/".$nbr_of_small.".txt";
					if(file_put_contents($filename,$ret) === false){
						echo "Failed to save SMALL doc nbr $nbr_of_small\n";
						$nbr_of_small--;
						continue;
					} else {
						echo "Saved SMALL docs: $nbr_of_small ($url)\n";
					}
					$small_pages[] = $url;
					continue;
				} else {				
					//Save file
					$nbr_of_saved++;
					$filename = $this->resPath."pages/".$nbr_of_saved.".txt";
					if(file_put_contents($filename,$ret) === false){
						echo "Failed to save doc nbr $nbr_of_saved\n";
						$nbr_of_saved--;
					} else {
						$saved_pages[] = $url;
						echo "Saved docs: $nbr_of_saved ($url)\n";
					}
				}
			}
			
		}
		file_put_contents($this->resPath."saved_urls.txt",implode("\n",$saved_urls));
		file_put_contents($this->resPath."small_pages_list.txt",implode("\n",$small_pages));
		echo "\nDone!\n-----------\n";
	}
}

$c = new Crawler($maPath,"martialarts");
//$c = new Crawler($coPath,"cooking");
//$c = new Crawler($chPath,"christianity");
$c->save_manual(300);
//$c = new Crawler("all","random");
//$c->save_random_docs("../dmoz_urls/url_list.txt",5000);

?>