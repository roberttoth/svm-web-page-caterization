<?php
require("includes/Page.php");

class Analyzer{

	//Create a file containing the mean term frequencies vs document length.
	//Format: doclength (in multiples of $bagsize), mean, percentile (given by the $percentile parameter)
	public static function create_weight_means_m_file($name,$ceiling,$bagsize=1){
		if(!file_exists("../results/$name/matlab")){
			if(!mkdir("../results/$name/matlab"))
				trigger_error("Could not create directory \"matlab\"", E_USER_ERROR);
			else 
				echo "Created folder \"matlab\"\n";
		}
		
		echo "Creating weight average m file...\n";
		$bags = array();

		$fileNbr = 0;
		$dir = new DirectoryIterator("../results/$name/pages/");
		echo "Reading files...\n";
		foreach($dir as $file){
			if($file->isDot()){
				continue;
			}	
			$p = new Page($file->getPathname(),$ceiling);
			$index = $p->sum_terms()/$bagsize;
			$p->remove_stop_words(true);
			foreach($p->bagOfWords as $count){
				if(!isset($bags[$index])){
					$bags[$index] = array();
				}
				$bags[$index][] = $count;
			}
			$fileNbr++;
			echo "Done with file nbr: $fileNbr         \r";
		}
		ksort($bags);		
		
		echo "Printing values to file...\n";
		$bagNbr = 0;
		$maxIndex = count($bags);
		
		if($ceiling < 0)
			$ceiling = "max";
		$out = fopen("../results/$name/Matlab/tf_means_".$ceiling.".m","w");
		fwrite($out, "means = [");
		foreach($bags as $index => $tfs){
			$sum = array_sum($tfs);
			$count = count($tfs);
			$mean = $sum/$count;
			fwrite($out, " $index $mean prctile([");
			foreach($tfs as $tf){
				fwrite($out, "$tf ");	
			}
			fwrite($out, "],[50 90 99]);");
			fwrite($out, "\n");
			$bagNbr++;
			echo "Done with bag nbr: $bagNbr out of $maxIndex        \r";
		}
		fwrite($out, "];\n");
		fwrite($out, "
		x = means(:,1);
		yMeans = means(:,2);
		yPercentile = means(:,3);
		modelFun = @(p,x) p*log(x);
		coefMeans = nlinfit(x, yMeans, modelFun, 0);
		coefPerc = nlinfit(x,yPercentile,modelFun,0);
		figure;
		plot(x,yMeans,'o');
		hold on;
		plot(x,coefMeans*log(x),'r');
		hold off;
		figure;
		plot(x,yPercentile,'+');
		hold on;
		plot(x,coefPerc*log(x),'r');
		");
		fclose($out);
	}
	
	//Each line in the resulting file contains all the weights from one weight vector (one document)
	public static function create_weight_distr_file($name,$ceiling,$inname,$outname,$only_label){
		if($ceiling < 0)
			$ceiling = "max";
		echo "Creating weight distribution file...\n";
		
		//Create folder if it does not exist
		if(!file_exists("../results/$name/matlab")){
			if(!mkdir("../results/$name/matlab"))
				trigger_error("Could not create directory \"matlab\"", E_USER_ERROR);
			else 
				echo "Created folder \"matlab\"\n";
		}
		
		//Check to see if the training file (to analyze) exists
		if(!file_exists("../results/$name/svm/".$inname)){
			trigger_error("File [$inname] does not exist\n",E_USER_ERROR);	
			return;
		}
		
		$in = fopen("../results/$name/svm/".$inname,"r");
		$out = fopen("../results/$name/matlab/".$outname,"w");
		
		$lineCount = 0;
		fwrite($out,"weights = [");
		while($line = fgets($in)){
			$label = trim(strstr($line," ",true));
			if(($only_label != "all") && ($label != $only_label)){
				$lineCount++;
				continue;
			} else {
				$line = trim(strstr($line," "));
				$pairs = explode(" ",$line);
				foreach($pairs as $pair){
					$vals = explode(":",$pair);
					fwrite($out," ".$vals[1]);
				}
				$lineCount++;
				echo "Done with line: $lineCount          \r";
			}
		}
		fwrite($out,"];\n");
		fwrite($out,"
		
		%%%%%%%
		%	Test effect of TF-ceiling
		%prc5 = prctile(weights5,[50 90 99]);
		%counts = hist(weights5,1000);
		%figure();
		%hold on;
		%hist(weights5,1000);
		%maxcount = max(counts);
		%maxweight = max(weights5);		
		%plot(prc5(1),maxcount/2,'r+');
		%plot(prc5(2),maxcount/2,'g+');
		%plot(prc5(3),maxcount/2,'k+');
		%legend('weights5',['median: ' num2str(prc5(1))],['90 prct: ' num2str(prc5(2))],['99 prct: ' num2str(prc5(3))]);
		%title('Weight distribution with 1000 bins and TF-ceiling of 5');
		%xlabel(['Weight per bin (max =  ' num2str(maxweight) ')']);
		%ylabel('Count per bin');
		%xlim([-0.01 (2*prc5(3))]);
		%clear;
		
		%%%%%%%
		%	Display probability distribution of weights
		maxweight = max(weights);
		scaled = weights./maxweight;
		maxscaled = max(scaled);
		len = length(scaled);
		bw = 1/len;
		centers = 0:bw:(1-bw);
		counts = hist(scaled,centers);
		prob = counts./len;
		maxprob = max(prob);	
		figure;
		hold on;
		bar(centers,prob,'hist');
		prc = prctile(scaled,[50 90 99]);
		plot(prc(1),maxprob/2,'r+');
		plot(prc(2),maxprob/2,'g+');
		plot(prc(3),maxprob/2,'y+');
		legend('Probabilities',['median: ' num2str(prc(1))],['90 prct: ' num2str(prc(2))],['99 prct: ' num2str(prc(3))]);
		title('Probability distribution for ".$name." (scaled to [0 1]) with TF-ceiling of ".$ceiling."');
		xlabel(['Weight (max =  ' num2str(maxscaled) ')']);
		ylabel('Probability');
		xlim([(-10*bw) (prc(3)*2)]);
		%clear;
		");
		
		
		
		
		fclose($out);
		fclose($in);
		echo "\nDone with $outname\n-----------\n";
	}
	
	public static function all_doc_len_distr(){
		echo "Collecting lengts...\n";
		$out =  fopen("../results/doc_lengths_15.txt","w");
		$names = array("cooking","christianity","martialarts","random");
		foreach($names as $name){
			for($i = 1; $i <= 5000; $i++){
				$page = new Page("../results/$name/pages/$i.txt",15);
				$page->remove_stop_words(true);
				$len = $page->sum_terms();
				fwrite($out,$len." ");
				echo "$name: $i         \r";
			}
		}
		fclose($out);
		"\nDone!\n";
	}
	
	public static function return_vectorSize_array($name, $collection, $vectors_file, $outfile ="tmp.txt"){
		$counts = array();
		if(file_exists($outfile)){
			echo "Found existing values file: $outfile\n";
			$rows = explode("\n",trim(file_get_contents($outfile)));
			foreach($rows as $row){
				$tmp = explode(" ",trim($row));
				$counts[$tmp[0]] = $tmp[1]; 
			}
			echo "Done!\n";
			$fiftypercentile = 0;
			for($pos = 50; $pos <= 100; $pos++){
				$fiftypercentile += $counts[$pos];
			}
			echo "50 percentile: ".round(($fiftypercentile/array_sum($counts))*100,0);
			return $counts;
		}
		$vectors = explode("\n",trim(file_get_contents($vectors_file)));
		$onFile = 0;
		foreach($vectors as $vector){
			$onFile++;
			$label = trim(substr($vector,0,2));
			$vector = trim(substr($vector,2));
			//echo "Label: $label, vector: $vector;\n";
			$vecCount = count(explode(" ",$vector));
			$page = new Page("$collection".$onFile.".txt",15);
			$page->remove_stop_words(true);
			$wbCount = count($page->bagOfWords);
			$tmp = round(($vecCount/$wbCount)*100,0);
			$counts[] = "".$tmp;
			if($tmp > 100)
				echo "WOAW WOAW WOAAAAW!\n";
			echo "Done with doc ".$onFile.", with $tmp%               \r";
			// if($onFile == 500)
				// break;
		}
		echo "\n";
		$counts = array_count_values($counts);
		$max = 100;
		for($index = 0; $index < $max; $index++){
			if(!isset($counts[$index])){
				$counts[$index] = 0;
			}
		}
		ksort($counts);
		echo "Done!\n";
		//DEBUG
		// echo "\n\n";
		// print_r(array_keys($counts));
		// echo "\n\n";
		$out = fopen($outfile,"w");
		foreach($counts as $pc => $count){
			fwrite($out,"$pc $count\n");
		}
		fclose($out);
		return $counts;
	}
	
	//Create two, space separated, files. One containing all the weights from a training file. 
	//The other contains all the feature numbers from the same training file.
	//For distribution analysis purposes
	public static function create_feature_and_weight_dumps($category,$label,$filename){
		echo "Starting <create_feature_and_weight_dumps> for $category with file $filename\n--------------\n";
		$counter = 0;
		$path = "../results/$category/svm/";
		$vectors = explode("\n",trim(file_get_contents($path.$filename)));
		$outWeights = fopen($path.strstr($filename,".",true).".weightsdump","w");
		$outFeatures = fopen($path.strstr($filename,".",true).".featuresdump","w");
		foreach($vectors as $vector){
			$local_label = trim(substr($vector,0,2));
			$vector = trim(substr($vector,2));
			if($local_label == $label){
				$counter++;
				echo "Doing line $counter                  \r";
				$pairs = explode(" ",trim($vector));
				foreach($pairs as $pair){
					$vals = explode(":",$pair);
					fwrite($outFeatures,$vals[0]." ");
					fwrite($outWeights,$vals[1]." ");
				}
			}
		}
		echo "\nDone!\n";
		fclose($outFeatures);
		fclose($outWeights);
	}
}
#create_weight_distr_file($name,$ceiling,$inname,$outname,$only_label)
// Analyzer::all_doc_len_distr();
//Analyzer::create_weight_distr_file("christianity",15,"train_rnd_from_rndp_complete_15.txt","wd_rndp_neg.m","-1");
//Analyzer::create_weight_means_m_file("martialarts",10,100);
//Analyzer::create_weight_distr_file("random",15,"_scaled_inv");
// Analyzer::create_weight_distr_file("christianity",5);
// Analyzer::create_weight_distr_file("christianity",10);
// Analyzer::create_weight_distr_file("christianity",15);
// Analyzer::create_weight_distr_file("christianity",-1);
// Analyzer::create_weight_distr_file("cooking",5);
// Analyzer::create_weight_distr_file("cooking",10);
// Analyzer::create_weight_distr_file("cooking",15);
// Analyzer::create_weight_distr_file("cooking",-1);

// Analyzer::create_weight_distr_file("martialarts",15);
// $positiveCollection = "C:/Users/robtot/Documents/exjobb/results/christianity/pages/";
// $negativeCollection = "C:/Users/robtot/Documents/exjobb/results/random/pages/";
// $category = "christianity";
// $positiveVectorsFile = "C:/Users/robtot/Documents/exjobb/results/christianity/svm/train_neg_from_random_15_complete.txt";
// $negativeVectorsFile = "C:/Users/robtot/Documents/exjobb/results/christianity/svm/pos_train_15_scaled.txt";

//Analyzer::create_feature_and_weight_dumps("christianity","-1","train_rnd_uniform_complete_15.txt");
?>