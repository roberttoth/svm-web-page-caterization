<?php

//Gets rid of every word that contains anything else than the letters a-z
/*! Needs the file idftab.txt !*/
//Produces the file idf_az.txt
function idf_cleanup(){
	$idfstream = fopen("idftab.txt", "r");
	$newidf = fopen("idf_az.txt", "w");
	$line = "";
	while(!feof($idfstream)){
		$line = trim(fgets($idfstream));
		$term = explode(" ", $line);
		if(preg_match("/^\"[a-z]+\"$/", $term[1]) == 1){
			$term[1] = trim($term[1], '"');
			fputs($newidf, "$term[0] $term[1]\n");
		}
	}
	fclose($idfstream);
	fclose($newidf);
}

//Remove stopwords defined in $swfile from the wordlist in $idffile
/*! The file $idffil is updated (information will be destroyed) !*/
function idf_rm_stopwords($idffile, $swfile){
	$idfstream = fopen($idffile, 'r');
	$swstream = fopen($swfile, "w");
	while(!feof($idfstream)){
		$line = trim(fgets($idfstream));
		$score = explode(" ", $line);
		if($score[0] < 0.3){
			fputs($swstream, "$score[1]\n");
		}
	}
	
	fclose($idfstream);
	fclose($swstream);
}

//Combine a list of stopwords with those found in includes/stop-words.php.
//The resulting new-line seperated list is save in stop-words-comb.txt
//The extra list used in this thesis was a list from the IDF table with values less than 0.3
function combine_sw_lists($list1){
	include("includes/stop-words.php");
	$l1 = file_get_contents($list1);
	$l1 = trim($l1);
	$sw1 = explode("\n" ,$l1);
	$combi = array_merge($stopwords,$sw1);
	$combi = array_unique($combi);
	$newsw = "";
	foreach($combi as $terms){
		$newsw .= "".$terms."\n";
	}
	file_put_contents("stop-words-comb.txt", $newsw);
}

?>