This README contains an introduction and description of how the code was used in the thesis 
"Selecting Negative Examples for Training an SVM Classifier" by Robert Toth. 
Please direct questions, comments and feedback to contact@arrtee.se
The code could still use some cleaning up, and there are many inconvenient dependencies between methods (such as expected files and folder structures).
But the code was never meant to be generally applicable, only to provide data for the thesis. It could, however, be generalized and tweaked 
without too much effort.

0. idfFilter -- Used for cleaning up the global IDF file

1. Crawler -- Fetching the documents for the collections:
	- Use the public methods in this class to create the training and testing collections:
	1. Create a instance of the crawler with the URL to the category and the name of the collection: $c = new Crawler("/Sports/Martial_Arts/","martialarts");
	2. get_subs() will fetch all the subcategories (and subsub etc)
	3. get_urls($maxUrlPerSub =-1) will extract all the URLs from the subcategories with the option of limiting the number of URLs from each sub.
	4. create_url_list() creates the (randomized) URL list that will actually be used. Index pages (index.html) are replaced by the first level URLs on that page.
		- 	This is because it is my oppinion that index-pages mostly act as a portal to the rest of the site and do not contain much text that helps in the 
			categorization task
	5. crawl_and_save($wantedAmount,$urlList = "good_urls.txt") actually downloads the documents from the URLlist created in the previous step (named good_urls.txt).
	   there is the option of specifying a different list. These documents are used as positive examples during training.
	6. save_manual($wantedAmount) will download the specified number of additional documents from the urls. This method uses the file "rest_urls.txt" which is 
	   the list of URLs that were NOT used for fetching the positive training examples. These document will be used for the tests and should be manually confirmed to 
	   be positive.
	7. save_random_docs($urllist,$wantedAmount) is really only used to create the collection of totally random pages from all the URLs on DMOZ. 
	   $urllist should therefor be the list created by the extract_all_rdf_urls($filepath,$outfile) in the Utilities.php file
	   
2. SVM -- Create the training data
	- The code is commented and the ideas are explained in the report.
	
3. Analyzer -- Create matlab-files for plotting the weight distributions of the different categories.

4. Utilities
	- This is a collection of utility methods and also the methods for collecting the negative documents that are to be manually categorized.
	  Special treatment was needed in order to get the right number of negative examples from each category that would be a good representation
	  of the distribution of pages among the different categories on DMOZ.
	  
A. Includes
	- This folder contains all the third-party code, the global IDF table, stop-words and the Page class.
	- Page -- this class is used to represent a document as a bag-of-words
	- PorterStemmer -- was supposed to be used for stemming but stemming was ommited.
	- strip-html-tags -- is also a very handy tool for removing invisible tags (tags that don't show up on the web-page and therefor probably
	does not contain any usefull information for the classification task. Eg. scripts,	styles etc)
	
	
Total files included:
Utilites.php
SVM.php
idfFilter.php
Crawler.php
Analyzer.php
includes/idftab.txt (From Entireweb)
includes/Page.php
includes/PorterStemmer.php (Copyright (c) 2005 Richard Heyes (http://www.phpguru.org/))
includes/stop-words.php
includes/strip_html_tags.php (Copyright (c) 2008, David R. Nadeau, NadeauSoftware.com)
includes/ReadMe.txt (readme for strip_html_tags.php)
includes/sw.txt (terms with values < 0.3 from idftab.txt)